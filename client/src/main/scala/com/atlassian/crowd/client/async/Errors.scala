package com.atlassian.crowd.client.async

/**
 * Error entities are the things that are transmitted as HTTP payload over the wire.
 */
case class ErrorEntity(reason: String, message: String) {

  def translate: Option[DecodedErrorEntity] = {
    val stringToDecodedErrorEntity: PartialFunction[String, DecodedErrorEntity] = {
      case "APPLICATION_ACCESS_DENIED" ⇒ ApplicationAccessDenied(message)
      case "APPLICATION_PERMISSION_DENIED" ⇒ ApplicationPermissionDenied(message)
      case "EXPIRED_CREDENTIAL" ⇒ ExpiredCredential(message)
      case "GROUP_NOT_FOUND" ⇒ GroupNotFound(message)
      case "ILLEGAL_ARGUMENT" ⇒ IllegalArgument(message)
      case "INACTIVE_ACCOUNT" ⇒ InactiveAccount(message)
      case "INVALID_USER_AUTHENTICATION" ⇒ InvalidUserAuthentication(message)
      case "INVALID_CREDENTIAL" ⇒ InvalidCredential(message)
      case "INVALID_EMAIL" ⇒ InvalidEmail(message)
      case "INVALID_GROUP" ⇒ InvalidGroup(message)
      case "INVALID_SSO_TOKEN" ⇒ InvalidSsoToken(message)
      case "INVALID_USER" ⇒ InvalidUser(message)
      case "MEMBERSHIP_NOT_FOUND" ⇒ MembershipNotFound(message)
      case "MEMBERSHIP_ALREADY_EXISTS" ⇒ MembershipAlreadyExists(message)
      case "NESTED_GROUPS_NOT_SUPPORTED" ⇒ NestedGroupsNotSupported(message)
      case "APPLICATION_NOT_FOUND" ⇒ ApplicationNotFound(message)
      case "UNSUPPORTED_OPERATION" ⇒ UnsupportedOperation(message)
      case "USER_NOT_FOUND" ⇒ UserNotFound(message)
      case "EVENT_TOKEN_EXPIRED" ⇒ EventTokenExpired(message)
      case "INCREMENTAL_SYNC_NOT_AVAILABLE" ⇒ IncrementalSyncNotAvailable(message)
      case "WEBHOOK_NOT_FOUND" ⇒ WebhookNotFound(message)
      case "PERMISSION_DENIED" ⇒ PermissionDenied(message)
      case "INVALID_MEMBERSHIP" ⇒ InvalidMembership(message)
    }
    stringToDecodedErrorEntity.lift(reason)
  }

}

// see com.atlassian.crowd.integration.rest.entity.ErrorEntity.ErrorReason for a list of errors

sealed trait DecodedErrorEntity

case class ApplicationAccessDenied(message: String) extends DecodedErrorEntity
case class ApplicationPermissionDenied(message: String) extends DecodedErrorEntity
case class ExpiredCredential(message: String) extends DecodedErrorEntity
case class GroupNotFound(message: String) extends DecodedErrorEntity
case class IllegalArgument(message: String) extends DecodedErrorEntity
case class InactiveAccount(message: String) extends DecodedErrorEntity
case class InvalidUserAuthentication(message: String) extends DecodedErrorEntity
case class InvalidCredential(message: String) extends DecodedErrorEntity
case object UserRequiresCaptcha extends DecodedErrorEntity
case class InvalidEmail(message: String) extends DecodedErrorEntity
case class InvalidGroup(message: String) extends DecodedErrorEntity
case class InvalidSsoToken(message: String) extends DecodedErrorEntity
case class InvalidUser(message: String) extends DecodedErrorEntity
case class MembershipNotFound(message: String) extends DecodedErrorEntity
case class MembershipAlreadyExists(message: String) extends DecodedErrorEntity
case class NestedGroupsNotSupported(message: String) extends DecodedErrorEntity
case class ApplicationNotFound(message: String) extends DecodedErrorEntity
case class UnsupportedOperation(message: String) extends DecodedErrorEntity
case class UserNotFound(message: String) extends DecodedErrorEntity
case class EventTokenExpired(message: String) extends DecodedErrorEntity
case class IncrementalSyncNotAvailable(message: String) extends DecodedErrorEntity
case class WebhookNotFound(message: String) extends DecodedErrorEntity
case class PermissionDenied(message: String) extends DecodedErrorEntity
case class InvalidMembership(message: String) extends DecodedErrorEntity

/**
 * Represents HTTP error messages that cannot be decoded by the Crowd client.
 */
case class RawError(body: String, headers: Map[String, String])

/**
 * Represents a failure in the Crowd client.
 */
sealed trait CrowdError

case class CrowdHttpError(statusCode: Int, errorEntity: Either[RawError, DecodedErrorEntity]) extends CrowdError
case object NotCrowdService extends CrowdError
case class ParseError(reason: String) extends CrowdError
case class NetworkError(reason: String, cause: Option[Throwable] = None) extends CrowdError
