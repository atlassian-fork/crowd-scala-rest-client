package com.atlassian.crowd.client.spray

import spray.json._
import com.atlassian.crowd.client.async._
import com.atlassian.crowd.client.model.{ Attributes, CookieConfiguration, EncryptedCredential, Group }
import com.atlassian.crowd.client.model.{ PlainTextCredential, SessionToken, User }
import com.atlassian.crowd.client.model.{ UserAuthenticationContext, UserAuthenticationContextWithoutPassword, UserAuthenticationContextWithPassword }
import com.atlassian.crowd.client.model.{ UserTemplate, UserWithAttributes, ValidationFactor }

object CrowdJsonProtocol extends DefaultJsonProtocol {

  implicit object userFormat extends RootJsonFormat[User] {
    def read(json: JsValue): User = {
      val jsObject: JsObject = json.asJsObject
      jsObject.getFields("name", "first-name", "last-name", "display-name", "email", "active") match {
        case Seq(JsString(name), JsString(firstName), JsString(lastName), JsString(displayName), JsString(email),
          JsBoolean(active)) ⇒ {
          // required fields have been matched, now match optional fields
          val maybeKey = jsObject.getFields("key").headOption.map {
            case JsString(key) ⇒ key
            case _ ⇒ throw new DeserializationException("Key is not a String")
          }
          val maybeUpdateDate = jsObject.getFields("updated-date").headOption.map {
            case JsNumber(updateDate) ⇒ updateDate.toLong
            case _ ⇒ throw new DeserializationException("Update date is not a number")
          }
          val createdDate = jsObject.getFields("created-date").headOption.map {
            case JsNumber(updateDate) ⇒ updateDate.toLong
            case _ ⇒ throw new DeserializationException("Created date is not a number")
          }

          User(name, firstName, lastName, displayName, email, active, maybeKey, createdDate, maybeUpdateDate)
        }
        case _ ⇒ throw new DeserializationException("User expected")
      }
    }

    def write(user: User): JsValue = {
      val fields = Map("name" -> JsString(user.name),
        "first-name" -> JsString(user.firstName),
        "last-name" -> JsString(user.lastName),
        "display-name" -> JsString(user.displayName),
        "email" -> JsString(user.email),
        "active" -> JsBoolean(user.active)) ++
        user.key.map("key" -> JsString(_)) ++
        user.updatedDate.map("updated-date" -> JsNumber(_)).toMap ++
        user.updatedDate.map("created-date" -> JsNumber(_)).toMap
      JsObject(fields)
    }
  }

  implicit object userWithAttributesFormat extends RootJsonReader[UserWithAttributes] {
    def read(json: JsValue): UserWithAttributes = {
      val user = userFormat.read(json)
      json.asJsObject.getFields("attributes") match {
        case Seq(JsObject(attributes)) ⇒ UserWithAttributes(user, attributesFormat.read(JsObject(attributes)))
        case _ ⇒ throw new DeserializationException("User with attributes expected")
      }
    }
  }

  implicit object attributesFormat extends RootJsonFormat[Attributes] {
    def read(json: JsValue): Attributes = json match {
      case JsObject(attributes) ⇒
        case class Attr(name: String, values: Set[String])
        implicit val attrFormat = jsonFormat2(Attr)
        val attrs = attributes.get("attributes")
        val res = attrs.map(_.convertTo[Set[Attr]].map(attr ⇒ attr.name → attr.values).toMap)
        Attributes(res.getOrElse(Map.empty))
      case _ ⇒ throw new DeserializationException("User attributes is not an object")
    }

    def write(attributes: Attributes): JsValue = {
      val attribArray = attributes.attributes.map { keyValueArray ⇒
        JsObject(
          "name" -> JsString(keyValueArray._1),
          "values" -> JsArray(keyValueArray._2.map(JsString(_)).toVector))
      }.toVector
      JsObject("attributes" -> JsArray(attribArray))
    }
  }

  implicit val groupFormat = jsonFormat3(Group.apply)

  class EntityListReader[A: JsonReader](entityName: String) extends RootJsonReader[List[A]] {
    def read(json: JsValue): List[A] =
      json.asJsObject.getFields(entityName) match {
        case Seq(JsArray(entities)) ⇒ entities.map(_.convertTo[A]).toList
        case _ ⇒ throw new DeserializationException("Entities array is absent")
      }
  }

  implicit object userListReader extends EntityListReader[User]("users")
  implicit object groupListReader extends EntityListReader[Group]("groups")

  class EntityNameListReader(entityName: String) extends RootJsonReader[List[String]] {
    def read(json: JsValue): List[String] =
      json.asJsObject.getFields(entityName) match {
        case Seq(JsArray(entities)) ⇒ entities.flatMap(_.asJsObject.getFields("name")).map {
          case JsString(name) ⇒ name
          case _ ⇒ throw new DeserializationException("Entity has no name")
        }.toList
        case _ ⇒ throw new DeserializationException("Entities array is absent")
      }
  }

  object userNameListReader extends EntityNameListReader("users")
  object groupNameListReader extends EntityNameListReader("groups")

  implicit val errorEntityFormat = jsonFormat2(ErrorEntity)

  implicit object userAuthenticationContextFormat extends RootJsonWriter[UserAuthenticationContext] {
    def write(context: UserAuthenticationContext): JsValue = context match {
      case UserAuthenticationContextWithPassword(username, passwordCredential, validationFactors) ⇒
        JsObject(requiredFields(username, validationFactors) +
          optionalPasswordField(passwordCredential))
      case UserAuthenticationContextWithoutPassword(username, validationFactors) ⇒
        JsObject(requiredFields(username, validationFactors))
    }

    private[this] def requiredFields(username: String, validationFactors: Seq[ValidationFactor]): Map[String, JsValue] = Map(
      "username" -> JsString(username),
      "validation-factors" -> writeValidationFactors(validationFactors))

    private[this] def optionalPasswordField(passwordCredential: PlainTextCredential): (String, JsValue) =
      "password" -> JsString(passwordCredential.credential)

    private[this] def writeValidationFactors(validationFactors: Seq[ValidationFactor]): JsObject =
      JsObject(validationFactors.map(_.asTuple).map {
        case (name, value) ⇒ name -> JsString(value)
      }.toMap)
  }

  implicit object sessionTokenFormat extends RootJsonFormat[SessionToken] {
    def write(sessionToken: SessionToken) =
      JsObject("token" -> JsString(sessionToken.token))

    def read(value: JsValue) =
      (value.asJsObject.getFields("token") match {
        case Seq(JsString(s)) => SessionToken.fromString(s)
        case _ => None
      }).getOrElse(deserializationError("Invalid session token"))
  }

  implicit val cookieConfigurationFormat = jsonFormat3(CookieConfiguration.apply)

  implicit object userTemplateFormat extends RootJsonWriter[UserTemplate] {
    def write(user: UserTemplate): JsValue = {
      val fields = Map("name" -> JsString(user.name),
        "first-name" -> JsString(user.firstName),
        "last-name" -> JsString(user.lastName),
        "display-name" -> JsString(user.displayName),
        "email" -> JsString(user.email),
        "active" -> JsBoolean(user.active),
        user.passwordCredential match {
          case EncryptedCredential(credential) ⇒ "encrypted-password" -> JsObject("value" -> JsString(credential))
          case PlainTextCredential(credential) ⇒ "password" -> JsObject("value" -> JsString(credential))
        },
        "attributes" -> attributesFormat.write(user.attributes))
      JsObject(fields)
    }
  }

  /**
   * Crowd requires validation factors in full ceremonial garb which is more trouble than it's worth implementing
   * via spray-json 's protocol stuff right now.
   */
  def validationFactorsAsCrowdJSON(validationFactors: Seq[ValidationFactor]): JsObject =
    JsObject.apply("validationFactors" -> JsArray(validationFactors.map { vf ⇒
      JsObject(Map(
        "name" -> JsString(vf.name),
        "value" -> JsString(vf.value)))
    }.toVector))

  implicit object plainTextCredentialWriter extends RootJsonWriter[PlainTextCredential] {
    def write(credential: PlainTextCredential): JsValue =
      JsObject("value" -> JsString(credential.credential))
  }
}
