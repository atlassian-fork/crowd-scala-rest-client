package com.atlassian.crowd.client.spray

import spray.json._
import org.specs2.mutable.SpecificationWithJUnit
import com.atlassian.crowd.client.model.{ Attributes, EncryptedCredential, PlainTextCredential, User }
import com.atlassian.crowd.client.model.{ UserAuthenticationContextWithPassword, UserAuthenticationContextWithoutPassword }
import com.atlassian.crowd.client.model.{ UserTemplate, ValidationFactors }

class CrowdJsonProtocolTest extends SpecificationWithJUnit {

  "The CrowdJsonProtocol" should {

    "parse a user without attributes" in {
      val json =
        """{ "name": "myname",
          |  "first-name": "myfname",
          |  "last-name": "mylname",
          |  "display-name": "mydisplayname",
          |  "email": "myemail",
          |  "key": "mykey",
          |  "updated-date": 1479349950986,
          |  "active": true
          |}
        """.stripMargin.parseJson
      val user = CrowdJsonProtocol.userFormat.read(json)
      user.name must_== "myname"
      user.firstName must_== "myfname"
      user.lastName must_== "mylname"
      user.displayName must_== "mydisplayname"
      user.email must_== "myemail"
      user.key must beSome("mykey")
      user.active must beTrue
    }

    "parse a user with attributes" in {
      val json =
        """{ "name": "myname",
          |  "first-name": "myfname",
          |  "last-name": "mylname",
          |  "display-name": "mydisplayname",
          |  "email": "myemail",
          |  "key": "mykey",
          |  "active": true,
          |  "updated-date": 1479349950986,
          |  "attributes": {
          |    "attributes": [
          |      {
          |        "name": "attr1",
          |        "values": [
          |          "attr1value1",
          |          "attr1value2"
          |        ]
          |      },
          |      {
          |        "name": "attr2",
          |        "values": [
          |          "attr2value1"
          |        ]
          |      }
          |    ]
          |  }
          |}
        """.stripMargin.parseJson
      val userWithAttributes = CrowdJsonProtocol.userWithAttributesFormat.read(json)
      userWithAttributes.name must_== "myname"
      userWithAttributes.firstName must_== "myfname"
      userWithAttributes.lastName must_== "mylname"
      userWithAttributes.displayName must_== "mydisplayname"
      userWithAttributes.email must_== "myemail"
      userWithAttributes.key must beSome("mykey")
      userWithAttributes.active must beTrue
      userWithAttributes.attributes must be_==(Attributes(Map("attr1" -> Set("attr1value1", "attr1value2"), "attr2" -> Set("attr2value1"))))
    }

    "parse attributes" in {
      val json =
        """{
          |  "attributes": [
          |    {
          |      "name": "attr1",
          |      "values": [
          |        "attr1value1",
          |        "attr1value2"
          |      ]
          |    },
          |    {
          |      "name": "attr2",
          |      "values": [
          |        "attr2value1"
          |      ]
          |    }
          |  ]
          |}
        """.stripMargin.parseJson
      val attributes = CrowdJsonProtocol.attributesFormat.read(json)
      attributes.attributes must be_==(Map("attr1" -> Set("attr1value1", "attr1value2"), "attr2" -> Set("attr2value1")))
    }

    "parse a user without key (Crowd < 2.7)" in {
      val json =
        """{ "name": "myname",
          |  "first-name": "myfname",
          |  "last-name": "mylname",
          |  "display-name": "mydisplayname",
          |  "email": "myemail",
          |  "active": true,
          |  "updated-date": 1479349950986
          |}
        """.stripMargin.parseJson
      val user = CrowdJsonProtocol.userFormat.read(json)
      user.name must_== "myname"
      user.firstName must_== "myfname"
      user.lastName must_== "mylname"
      user.displayName must_== "mydisplayname"
      user.email must_== "myemail"
      user.key must beNone
      user.active must beTrue
    }

    "fail to parse user with an invalid key" in {
      val json =
        """{ "name": "myname",
          |  "first-name": "myfname",
          |  "last-name": "mylname",
          |  "display-name": "mydisplayname",
          |  "email": "myemail",
          |  "key": true,
          |  "active": true
          |}
        """.stripMargin.parseJson
      CrowdJsonProtocol.userFormat.read(json) must throwA[DeserializationException]
    }

    "serialise a user without attributes" in {
      val user = new User("myname", "myfname", "mylname", "mydisplayname", "myemail", true, Some("mykey"), Some(1479349950985l), Some(1479349950986l))
      val json = CrowdJsonProtocol.userFormat.write(user).asJsObject
      json.getFields("name", "first-name", "last-name", "display-name", "email", "key", "active", "updated-date") must_== Seq(
        JsString("myname"), JsString("myfname"), JsString("mylname"), JsString("mydisplayname"), JsString("myemail"),
        JsString("mykey"), JsBoolean(true), JsNumber(1479349950986l))
    }

    "serialise all attributes" in {
      val attributes = Attributes(Map("attr1" -> Set("attr1value1", "attr1value2"), "attr2" -> Set("attr2value1")))

      val json = CrowdJsonProtocol.attributesFormat.write(attributes).asJsObject
      json.getFields("attributes") match {
        case Seq(JsArray(attribs)) ⇒ attribs must have size 2
      }
    }

    "serialise multi-valued attributes" in {
      val attributes = Attributes(Map("attr1" -> Set("attr1value1", "attr1value2")))

      val json = CrowdJsonProtocol.attributesFormat.write(attributes).asJsObject
      json.getFields("attributes") match {
        case Seq(JsArray(Vector(JsObject(map)))) ⇒ map must havePair("values" -> JsArray(JsString("attr1value1"), JsString("attr1value2")))
      }
    }

    "serialise user authentication context with password" in {
      val context = new UserAuthenticationContextWithPassword("myname", PlainTextCredential("mypassword"),
        Seq(ValidationFactors.CustomValidationFactor("k1", "v1")))
      val json = CrowdJsonProtocol.userAuthenticationContextFormat.write(context).asJsObject
      json.getFields("username", "password", "validation-factors") must_== Seq(
        JsString("myname"),
        JsString("mypassword"),
        JsObject(Map("k1" -> JsString("v1"))))
    }

    "serialise user authentication context without a password" in {
      val context = new UserAuthenticationContextWithoutPassword("myname",
        Seq(ValidationFactors.CustomValidationFactor("k1", "v1")))
      val json = CrowdJsonProtocol.userAuthenticationContextFormat.write(context).asJsObject
      json.getFields("username", "validation-factors") must_== Seq(
        JsString("myname"),
        JsObject(Map("k1" -> JsString("v1"))))
      json.getFields("password") must beEmpty
    }

    "serialise user template" in {
      val name = "myname"
      val firstName = "myfname"
      val lastName = "mylname"
      val displayName = "mydisplayname"
      val email = "myemail"
      val active = true
      val password = "secret"
      val attributes = Attributes(Map("attr1" -> Set("attr1value1")))

      def createJson(encryptedPassword: Boolean) = {
        val userTemplate = UserTemplate(name = name,
          firstName = firstName,
          lastName = lastName,
          displayName = displayName,
          email = email,
          active = true,
          passwordCredential = if (encryptedPassword) EncryptedCredential(password) else PlainTextCredential(password),
          attributes = attributes)
        CrowdJsonProtocol.userTemplateFormat.write(userTemplate).asJsObject
      }

      "with encrypted password" should {
        val json = createJson(encryptedPassword = true)

        "contain the template fields" in {
          json.getFields("name",
            "first-name",
            "last-name",
            "display-name",
            "email",
            "active",
            "encrypted-password",
            "attributes") must_== Seq(
              JsString(name),
              JsString(firstName),
              JsString(lastName),
              JsString(displayName),
              JsString(email),
              JsBoolean(active),
              JsObject(Map("value" -> JsString(password))),
              JsObject(Map("attributes" -> JsArray(JsObject(Map("name" -> JsString("attr1"), "values" -> JsArray(JsString("attr1value1"))))))))
        }

        "not contain the password field" in {
          json.getFields("password") must_== Seq()
        }
      }

      "with plain-text password" should {
        val json = createJson(encryptedPassword = false)

        "contain the template fields" in {
          json.getFields("name",
            "first-name",
            "last-name",
            "display-name",
            "email",
            "active",
            "password",
            "attributes") must_== Seq(
              JsString(name),
              JsString(firstName),
              JsString(lastName),
              JsString(displayName),
              JsString(email),
              JsBoolean(active),
              JsObject(Map("value" -> JsString(password))),
              JsObject(Map("attributes" -> JsArray(JsObject(Map("name" -> JsString("attr1"), "values" -> JsArray(JsString("attr1value1"))))))))
        }

        "not contain the encrypted-password field" in {
          json.getFields("encrypted-password") must_== Seq()
        }
      }
    }

    "entity list parsing" should {
      "parse a list of entities" in {
        val json =
          """
            |{
            |  "expand": "fruit",
            |  "fruits": [ "apple", "orange" ]
            |}
          """.stripMargin.parseJson
        implicit val stringFormat = DefaultJsonProtocol.StringJsonFormat
        val entities = new CrowdJsonProtocol.EntityListReader[String]("fruits").read(json)
        entities must containTheSameElementsAs(Seq("apple", "orange"))
      }

      "parse an empty list of entities" in {
        val json = JsObject("expand" -> JsString("fruit"), "fruits" -> JsArray())
        implicit val stringFormat = DefaultJsonProtocol.StringJsonFormat
        val entities = new CrowdJsonProtocol.EntityListReader[String]("fruits").read(json)
        entities must beEmpty
      }
    }

    "entity name list parsing" should {
      "parse a list of entity names" in {
        val json =
          """
            |{
            |  "expand": "fruit",
            |  "fruits": [
            |    { "name": "apple" },
            |    { "name": "orange" }
            |  ]
            |}
          """.stripMargin.parseJson
        val entities = new CrowdJsonProtocol.EntityNameListReader("fruits").read(json)
        entities must containTheSameElementsAs(Seq("apple", "orange"))
      }
      "parse an empty list of entity names" in {
        val json = JsObject("expand" -> JsString("fruit"), "fruits" -> JsArray())
        val entities = new CrowdJsonProtocol.EntityNameListReader("fruits").read(json)
        entities must beEmpty
      }
    }
  }
}
