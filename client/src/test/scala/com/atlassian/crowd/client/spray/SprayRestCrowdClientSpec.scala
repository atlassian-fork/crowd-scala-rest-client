package com.atlassian.crowd.client.spray

import akka.actor.ActorSystem
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.unmarshalling.{ FromResponseUnmarshaller, Unmarshal, Unmarshaller }
import akka.stream.{ ActorMaterializer, Materializer }
import akka.testkit.TestKit
import com.atlassian.crowd.client.async._
import com.atlassian.crowd.client.model._
import org.specs2.ScalaCheck
import org.specs2.concurrent.ExecutionEnv
import org.specs2.matcher.FutureMatchers
import org.specs2.mock.Mockito
import org.specs2.mutable.SpecificationWithJUnit
import org.specs2.specification.Scope
import spray.json.{ JsArray, JsBoolean, JsNumber, JsObject, JsString, JsValue }
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import scala.concurrent.{ ExecutionContext, Future }

class CrowdResponseTransformersSpec(implicit ev: ExecutionEnv) extends SpecificationWithJUnit with Mockito {
  private implicit val system: ActorSystem = ActorSystem("test")
  private implicit val materializer: Materializer = ActorMaterializer()

  object sut extends CrowdResponseTransformers

  "The Crowd service validator" should {
    "fail if the header X-Embedded-Crowd-Version is not present in the response" in {
      val httpResponse = HttpResponse()
      val result = sut.validateCrowdService(httpResponse)
      result must beLeft(NotCrowdService: CrowdError)
    }
    "validate the response if it contains the header X-Embedded-Crowd-Version" in {
      val httpResponse = HttpResponse(headers = List(RawHeader("X-Embedded-Crowd-Version", "value-is-not-relevant")))
      val result = sut.validateCrowdService(httpResponse)
      result must beRight(httpResponse)
    }
  }

  "The ErrorEntity translator" should {
    "let the response pass through if it is valid" in {
      val httpResponse = HttpResponse()
      val result = sut.translateErrorEntity.apply(Right(httpResponse))
      result must beRight(httpResponse).await
    }
    "let the error pass if there is a previous error" in {
      val crowdError = mock[CrowdError]
      val result = sut.translateErrorEntity.apply(Left(crowdError): Either[CrowdError, HttpResponse])
      result must beLeft(crowdError).await
    }
    "parse the ErrorEntity and translate it into a CrowdError" in {
      val httpResponse = HttpResponse(status = StatusCodes.NotFound, entity = HttpEntity(ContentTypes.`application/json`,
        """{
            "reason": "USER_NOT_FOUND",
            "message": "User <alice> does not exist"
        }"""))
      val result = sut.translateErrorEntity.apply(Right(httpResponse))
      result must beLeft[CrowdError](CrowdHttpError(404, Right(UserNotFound("User <alice> does not exist")))).await
    }
    "fail if the ErrorEntity does not exist" in {
      val httpResponse = HttpResponse(status = StatusCodes.NotFound)
      val result = sut.translateErrorEntity.apply(Right(httpResponse))
      result must beLeft[CrowdError].like {
        case CrowdHttpError(404, Left(RawError(_, returnedHeaders))) ⇒
          returnedHeaders must_== Map()
      }.await
    }
    "fail if the ErrorEntity cannot be parsed" in {
      val httpResponse = HttpResponse(status = StatusCodes.NotFound,
        headers = List(RawHeader("color", "red")),
        entity = HttpEntity("body"))
      val result = sut.translateErrorEntity.apply(Right(httpResponse))
      result must beLeft[CrowdError].like {
        case CrowdHttpError(404, Left(RawError(_, returnedHeaders))) ⇒
          returnedHeaders must_== Map("color" -> "red")
      }.await
    }
  }
}

class CrowdServer extends TestKit(ActorSystem("crowd-server-test-system")) with Scope {
  private implicit val materializer: Materializer = ActorMaterializer()

  def after() = system.terminate()

  val baseUri = Uri("http://example.test")
  val appName = "appName"
  val appPassword = "appPassword"

  val responseHeaders = List(RawHeader("X-Embedded-Crowd-Version", "value-is-not-relevant"))

  def realCrowdServer(implicit executionContext: ExecutionContext) =
    new SprayRestCrowdClient(baseUri, appName, appPassword)

  def mockCrowdServer(serverFunction: HttpRequest ⇒ HttpResponse)(implicit executionContext: ExecutionContext) =
    new SprayRestCrowdClient(baseUri, appName, appPassword) {
      override def sendReceive = (Future.successful[HttpResponse] _) compose serverFunction
    }

  def errorEntity(reason: String, message: String): ResponseEntity =
    HttpEntity(ContentTypes.`application/json`,
      s"""{
            "reason": "$reason",
            "message": "$message"
        }""")

}

class SprayRestCrowdClientRequestsTest(implicit ev: ExecutionEnv) extends SpecificationWithJUnit with ScalaCheck with FutureMatchers {
  private implicit val system: ActorSystem = ActorSystem("test")
  private implicit val materializer: Materializer = ActorMaterializer()

  object sut extends {
    val crowdBaseUri = Uri("http://example.test")
  } with SprayRestCrowdClientRequests

  def haveQueryParameters(parameters: (String, String)*) =
    contain(allOf(parameters: _*))

  "get an existing user by name" in {
    val CrowdRequest(request, _) = sut.getUserByName("myname")
    request must beLike {
      case HttpRequest(HttpMethods.GET, uri, _, _, _) ⇒
        uri.path must be_===(Uri.Path("/rest/usermanagement/1/user"))
        uri.query() must be_===(Uri.Query("username" -> "myname"))
    }
  }

  "get an existing user with attributes by name" in {
    val CrowdRequest(request, _) = sut.getUserWithAttributes("myname")
    request must beLike {
      case HttpRequest(HttpMethods.GET, uri, _, _, _) ⇒
        uri.path must be_===(Uri.Path("/rest/usermanagement/1/user"))
        uri.query() must be_===(Uri.Query("username" -> "myname", "expand" -> "attributes"))
    }
  }

  "get an existing user by key" in {
    val CrowdRequest(request, _) = sut.getUserByKey("mykey")
    request must beLike {
      case HttpRequest(HttpMethods.GET, uri, _, _, _) ⇒
        uri.path must be_===(Uri.Path("/rest/usermanagement/1/user"))
        uri.query() must be_===(Uri.Query("key" -> "mykey"))
    }
  }

  "create a new user" in {
    val name = "myname"
    val firstName = "FirstName"
    val lastName = "LastName"
    val displayName = "Display Name"
    val email = "myemail"
    val active = true
    val password = "mypassword"
    val attributes = Attributes(Map("attr1" -> Set("attr1value1")))
    val newUser = UserTemplate(name = name, firstName = firstName, lastName = lastName, displayName = displayName, email = email, active = active, PlainTextCredential(password), attributes = attributes)
    val expectedUserTemplate: JsValue = JsObject("name" -> JsString(name),
      "first-name" -> JsString(firstName),
      "last-name" -> JsString(lastName),
      "display-name" -> JsString(displayName),
      "email" -> JsString(email),
      "active" -> JsBoolean(active),
      "password" -> JsObject("value" -> JsString(password)),
      "attributes" -> JsObject(Map("attributes" -> JsArray(JsObject(Map("name" -> JsString("attr1"), "values" -> JsArray(JsString("attr1value1"))))))))

    val CrowdRequest(request, _) = sut.createUser(newUser)
    request must beLike {
      case HttpRequest(HttpMethods.POST, Uri(_, _, Uri.Path("/rest/usermanagement/1/user"), _, _), _, entity, _) ⇒
        Unmarshal(entity).to[JsValue] must be_===(expectedUserTemplate).await
    }
  }

  "modify a user" in {
    val user = User("name", "ofname", "olname", "odname", "oemail", active = true, Some("key"), Some(1479349950985l), Some(1479349950986l))

    def entityForUser(user: User): JsValue = JsObject(
      "name" -> JsString(user.name),
      "first-name" -> JsString(user.firstName),
      "last-name" -> JsString(user.lastName),
      "display-name" -> JsString(user.displayName),
      "email" -> JsString(user.email),
      "active" -> JsBoolean(user.active),
      "updated-date" -> JsNumber(1479349950986l),
      "created-date" -> JsNumber(1479349950986l))

    def putUserEntityMatching(expected: User) =
      beLike[HttpRequest] {
        case HttpRequest(HttpMethods.PUT, uri @ Uri(_, _, Uri.Path("/rest/usermanagement/1/user"), query, _), _, entity, _) ⇒
          uri.query().get("username") must beSome(user.name)
          Unmarshal(entity).to[JsValue] must be_===(entityForUser(expected)).await
      }

    "when setting first name" in {
      val newFirstName = "nfname"
      val CrowdRequest(request, _) = sut.updateUser(user, newFirstName = Some(newFirstName))
      request must putUserEntityMatching(user.copy(firstName = newFirstName))
    }

    "when setting last name" in {
      val newLastName = "nlname"
      val CrowdRequest(request, _) = sut.updateUser(user, newLastName = Some(newLastName))
      request must putUserEntityMatching(user.copy(lastName = newLastName))
    }

    "when setting display name" in {
      val newDisplayName = "ndname"
      val CrowdRequest(request, _) = sut.updateUser(user, newDisplayName = Some(newDisplayName))
      request must putUserEntityMatching(user.copy(displayName = newDisplayName))
    }

    "when setting email" in {
      val newEmail = "nemail"
      val CrowdRequest(request, _) = sut.updateUser(user, newEmail = Some(newEmail))
      request must putUserEntityMatching(user.copy(email = newEmail))
    }

    "when setting active state" in {
      val newActive = false
      val CrowdRequest(request, _) = sut.updateUser(user, newActive = Some(newActive))
      request must putUserEntityMatching(user.copy(active = newActive))
    }

  }

  "set a users's password" in {
    val username = "name"
    val password = "password"
    val expectedEntity: JsValue = JsObject("value" -> JsString(password))

    val CrowdRequest(request, _) = sut.setPassword(username, PlainTextCredential(password))
    request must beLike {
      case HttpRequest(HttpMethods.PUT, uri @ Uri(_, _, Uri.Path("/rest/usermanagement/1/user/password"), query, _), _, entity, _) ⇒
        uri.query().get("username") must beSome(username)
        Unmarshal(entity).to[JsValue] must be_===(expectedEntity).await
    }
  }

  "change users's username" in {
    val username = "name"
    val newUsername = "newName"
    val expectedEntity: JsValue = JsObject("new-name" -> JsString(newUsername))

    val CrowdRequest(request, _) = sut.renameUser(username, newUsername)
    request must beLike {
      case HttpRequest(HttpMethods.POST, uri @ Uri(_, _, Uri.Path("/rest/usermanagement/1/user/rename"), query, _), _, entity, _) ⇒
        uri.query().get("username") must beSome(username)
        Unmarshal(entity).to[JsValue] must be_===(expectedEntity).await
    }
  }

  "merge user attributes" in {
    val username = "myname"
    val attribs = Map("attrib1" -> Set("a", "b"), "attrib2" -> Set("c"))
    val expectedJson: JsValue = JsObject("attributes" -> JsArray(
      JsObject("name" -> JsString("attrib1"), "values" -> JsArray(JsString("a"), JsString("b"))),
      JsObject("name" -> JsString("attrib2"), "values" -> JsArray(JsString("c")))))

    val CrowdRequest(request, _) = sut.mergeUserAttributes(username, attribs)
    request must beLike {
      case HttpRequest(HttpMethods.POST, uri, _, entity, _) ⇒
        uri.path must be_===(Uri.Path("/rest/usermanagement/1/user/attribute"))
        uri.query() must be_===(Uri.Query("username" -> username))
        Unmarshal(entity).to[JsValue] must be_===(expectedJson).await
    }
  }

  "remove user attributes" in {
    val username = "myname"
    val attributeName = "myattribute"

    val CrowdRequest(request, _) = sut.removeUserAttributes(username, attributeName)
    request must beLike {
      case HttpRequest(HttpMethods.DELETE, uri, _, _, _) ⇒
        uri.path must be_===(Uri.Path("/rest/usermanagement/1/user/attribute"))
        uri.query() must be_===(Uri.Query("username" -> username, "attributename" -> attributeName))
    }
  }

  "delete an existing user" in {
    val CrowdRequest(request, _) = sut.removeUser("myname")
    request must beLike {
      case HttpRequest(HttpMethods.DELETE, uri, _, entity, _) ⇒
        uri.path must be_===(Uri.Path("/rest/usermanagement/1/user"))
        uri.query() must be_===(Uri.Query("username" -> "myname"))
        entity must be_===(HttpEntity.Empty)
    }
  }

  "searching for users" in {
    val cql = """lastName = Smith or email = "smith@example.com""""
    val CrowdRequest(request, _) = sut.searchUsers(cql, 15, 30)
    "have correct method" in {
      request.method must_== HttpMethods.GET
    }
    "have a uri with" in {
      "the correct path" in {
        request.uri.path must_== Uri.Path("/rest/usermanagement/1/search")
      }
      "a correct query" in {
        request.uri.query() must haveQueryParameters(
          "entity-type" -> "user",
          "expand" -> "user",
          "start-index" -> "15",
          "max-results" -> "30",
          "restriction" -> cql)
      }
    }
  }

  "validating password" in {
    val crowdRequest = sut.validatePassword("user name", "password")
    val CrowdRequest(request, _) = crowdRequest

    "have the correct method" in {
      request.method must_== HttpMethods.POST
    }
    "have a uri with" in {
      "the correct path" in {
        request.uri.path must_== Uri.Path("/rest/usermanagement/1/authentication")
      }
      "the correct query" in {
        request.uri.query() must haveQueryParameters("username" -> "user name")
      }
    }
    "have the password in the entity" in {
      Unmarshal(request.entity).to[JsValue] must be_===(JsObject("value" -> JsString("password")): JsValue).await
    }
  }
}

class SprayRestCrowdClientTest(implicit ev: ExecutionEnv) extends SpecificationWithJUnit {
  import spray.json.DefaultJsonProtocol._
  import akka.http.scaladsl.client.RequestBuilding._
  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

  case class Foo(foo: String)
  private implicit val fooFormat = jsonFormat1(Foo)

  private def mkRequest[A: FromResponseUnmarshaller](req: HttpRequest): CrowdRequest[A] =
    CrowdRequest(req, implicitly[FromResponseUnmarshaller[A]])

  "The client" should {
    implicit val executionContext = scala.concurrent.ExecutionContext.global

    "handle OK responses" in new CrowdServer {
      val client = mockCrowdServer { _ ⇒
        HttpResponse(status = StatusCodes.OK, headers = responseHeaders,
          entity = HttpEntity(ContentTypes.`application/json`, """{ "foo": "bar" }"""))
      }
      val request = mkRequest[Foo](Get(Uri("http://foo.example")))
      client.performRequest(request) must beRight.await
    }

    "handle empty responses" in new CrowdServer {
      val client = mockCrowdServer { _ ⇒
        HttpResponse(status = StatusCodes.OK, headers = responseHeaders,
          entity = HttpEntity.Empty)
      }
      val request = mkRequest[Unit](Delete(Uri("http://foo.example")))
      client.performRequest(request) must beRight(()).await
    }

    "be tolerant with unexpected non-empty responses for forward compatibility" in new CrowdServer {
      val client = mockCrowdServer { _ ⇒
        HttpResponse(status = StatusCodes.OK, headers = responseHeaders,
          entity = HttpEntity("non-empty"))
      }
      val request = mkRequest[Unit](Delete(Uri("http://foo.example")))
      client.performRequest(request) must beRight(()).await
    }

    "handle user not found" in new CrowdServer {
      val client = mockCrowdServer { _ ⇒
        HttpResponse(status = StatusCodes.NotFound, headers = responseHeaders,
          entity = errorEntity("USER_NOT_FOUND", "user <myname> does not exist"))
      }
      val request = mkRequest[Foo](Get(Uri("http://foo.example")))
      client.performRequest(request) must beLeft[CrowdError].like {
        case CrowdHttpError(404, Right(UserNotFound(text))) ⇒
          text must_== "user <myname> does not exist"
      }.await
    }

    "handle group not found" in new CrowdServer {
      val client = mockCrowdServer { _ ⇒
        HttpResponse(status = StatusCodes.NotFound, headers = responseHeaders,
          entity = errorEntity("GROUP_NOT_FOUND", "group <myname> does not exist"))
      }
      val request = mkRequest[Foo](Get(Uri("http://foo.example")))
      client.performRequest(request) must beLeft[CrowdError].like {
        case CrowdHttpError(404, Right(GroupNotFound(text))) ⇒
          text must_== "group <myname> does not exist"
      }.await
    }

    "handle invalid user authentication" in new CrowdServer {
      val client = mockCrowdServer { _ ⇒
        HttpResponse(status = StatusCodes.NotFound, headers = responseHeaders,
          entity = errorEntity("INVALID_USER_AUTHENTICATION", "gtfo"))
      }
      val request = mkRequest[Foo](Get(Uri("http://foo.example")))
      client.performRequest(request) must beLeft[CrowdError].like {
        case CrowdHttpError(404, Right(InvalidUserAuthentication(text))) ⇒
          text must_== "gtfo"
      }.await
    }

    "handle invalid sso tokens" in new CrowdServer {
      val client = mockCrowdServer { _ ⇒
        HttpResponse(status = StatusCodes.NotFound, headers = responseHeaders,
          entity = errorEntity("INVALID_SSO_TOKEN", "lol nope"))
      }
      val request = mkRequest[Foo](Get(Uri("http://foo.example")))
      client.performRequest(request) must beLeft[CrowdError].like {
        case CrowdHttpError(404, Right(InvalidSsoToken(text))) ⇒
          text must_== "lol nope"
      }.await
    }
  }

  private implicit val emptyResponseBodyAsUnit: Unmarshaller[HttpEntity, Unit] = Unmarshaller(_ => _ => Future.successful(()))
}
