name := "crowd-scala-client-model"

libraryDependencies ++= Seq(
  "com.github.julien-truffaut"  %% "monocle-core"      % Versions.monocle,
  "com.github.julien-truffaut"  %% "monocle-law"       % Versions.monocle  % Test,
  "com.github.julien-truffaut"  %% "monocle-macro"     % Versions.monocle,
  "org.scala-lang"              %  "scala-library"     % scalaVersion.value,
  "org.specs2"                  %% "specs2-core"       % Versions.specs2   % Test,
  "org.specs2"                  %% "specs2-scalacheck" % Versions.specs2   % Test
)

addCompilerPlugin("org.scalamacros" %% "paradise" % "2.1.0" cross CrossVersion.full)

scalariformSettings
