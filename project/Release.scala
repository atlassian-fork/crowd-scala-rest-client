import sbt._
import sbt.Process
import sbtrelease.ReleasePlugin.{ReleaseKeys, releaseSettings}
import sbtrelease.ReleasePlugin.ReleaseKeys._
import sbtrelease.ReleaseStateTransformations._
import sbtrelease._
import sbt.Keys._

object Release {

  private lazy val releaseProcess = Seq[ReleaseStep](
    checkSnapshotDependencies,
    inquireVersions,
    runClean,

    setReleaseVersion,
    stageFilesWithModifiedVersions,
    commitReleaseVersion,

    tagRelease,

    publishArtifacts,

    setNextVersion,
    stageFilesWithModifiedVersions,
    commitNextVersion,

    pushChanges
  )

  /* --- Release Steps --- */

  private def vcs(st: State): Vcs = {
    import Utilities._
    st.extract.get(versionControlSystem).getOrElse(sys.error("Aborting release. Working directory is not a repository of a recognized VCS."))
  }

  private lazy val stageFilesWithModifiedVersions = ReleaseStep { st =>
    vcs(st).cmd("add", "-u") !! st.log
    st
  }

  private lazy val customVcsMessages = Seq(
    tagComment <<= (version in ThisBuild) map { v => "[sbt-release] Releasing %s" format v },
    commitMessage <<= (version in ThisBuild) map { v => "[sbt-release] Setting version to %s" format v }
  )

  private def systemProperty(propName: String): Option[String] =
    Option(System.getProperty(propName, "")).filter(!_.isEmpty)

  private def defaultReleaseVersion(currentVer: String): Option[Version] =
    Version(currentVer).map(_.withoutQualifier)

  private def defaultNextVersion(releaseVer: String, bumpType: Version.Bump): Option[Version] =
    Version(releaseVer).map(_.bump(bumpType).asSnapshot)

  private def systemPropertyVersionOrDefault(propName: String, defaultVersion: => Option[Version]): String =
    systemProperty(propName).flatMap(Version(_))
      .orElse(defaultVersion)
      .getOrElse(versionFormatError).string

  lazy val customReleaseSettings =
    releaseSettings ++
      Seq(
        ReleaseKeys.releaseProcess := Release.releaseProcess,
        releaseVersion := { currentVer => systemPropertyVersionOrDefault("sbt.release.version", defaultReleaseVersion(currentVer)) },
        nextVersion := { releaseVer => systemPropertyVersionOrDefault("sbt.next.version", defaultNextVersion(releaseVer, versionBump.value)) }
      ) ++
      customVcsMessages
}